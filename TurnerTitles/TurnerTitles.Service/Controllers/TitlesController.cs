﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TurnerTitles.Data;

namespace TurnerTitles.Service.Controllers
{
    public class TitlesController : ApiController
    {
        Title[] titles = new Title[]
        {
            new Title {TitleId = 1, TitleName = "Ben Hur", ReleaseYear=1990 },
            new Title {TitleId = 2, TitleName = "Few Good Men", ReleaseYear=1995 },
            new Title {TitleId = 3, TitleName = "Honor Among Theives", ReleaseYear=1997 },
        };

        public IEnumerable<Title> GetAllTitles()
        {           
            try
            {
                using (var ctx = new TurnerTitlesContext())
                {
                    ctx.Configuration.ProxyCreationEnabled = false;
                    var titles = ctx.Titles.ToList();
                    return titles;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }                           
        }

        public IEnumerable<Title> GetSpecificTitles(string title)
        {
            try
            {
                using (var ctx = new TurnerTitlesContext())
                {
                    ctx.Configuration.ProxyCreationEnabled = false;
                    var searchedTitles = ctx.Titles.Where(t => t.TitleName.Contains(title)).OrderBy(t => t.TitleNameSortable).ToList();
                    return searchedTitles;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

    }
}
