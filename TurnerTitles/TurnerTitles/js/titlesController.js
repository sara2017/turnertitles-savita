﻿/// <reference path="titlesController.js" />
// titlesController.js
(function () {

    "use strict";

    angular.module("app-turner")
        .controller("titlesController", titlesController);

    function titlesController($http) {
        var vm = this;

        vm.titles = [];
        vm.searchString = {};
        vm.errorMessage = "";
        vm.showTitles = true;
        vm.showDetails = true;
        vm.inTitle = "";
        
        vm.currTitleName = "";
        vm.awards = [];
        vm.genres = [];
        vm.storylines = [];
        vm.participants = [];

        vm.findTitle = function () {
            //alert(vm.searchString.name);
            vm.inTitle = vm.searchString.name;
            $http.get("/api/Titles/SpecificTitles?title=" + vm.inTitle)
            .then(function (response) {
                angular.copy(response.data, vm.titles);
            },
            function (error) {
                vm.errorMessage = "Some error occured in Titles: " + error;
            });
        };

        vm.getDetail = function (titleId, titleName) {
            vm.currTitleName = titleName;
            $http.get("/api/Titles/Awards?titleId=" + titleId)
            .then(function (response) {
                angular.copy(response.data, vm.awards);
            },
            function (error) {
                vm.errorMessage = "Some error occured in Awards: " + error;
            });

            $http.get("/api/Titles/Storylines?titleId=" + titleId)
            .then(function (response) {
                angular.copy(response.data, vm.storylines);
            },
            function (error) {
                vm.errorMessage = "Some error occured in Storylines: " + error;
            });

            $http.get("/api/Titles/Genres?titleId=" + titleId)
            .then(function (response) {
                angular.copy(response.data, vm.genres);
            },
            function (error) {
                vm.errorMessage = "Some error occured in Genres: " + error;
            });

            $http.get("/api/Titles/Participants?titleId=" + titleId)
            .then(function (response) {
                angular.copy(response.data, vm.participants);
            },
            function (error) {
                vm.errorMessage = "Some error occured in Participants: " + error;
            });

        };
    }
})();