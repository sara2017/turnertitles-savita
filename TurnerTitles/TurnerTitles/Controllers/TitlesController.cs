﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TurnerTitles.Data;

namespace TurnerTitles.Service.Controllers
{
    public class TitlesController : ApiController
    {
        public IEnumerable<Title> GetAllTitles()
        {           
            try
            {
                using (var ctx = new TurnerTitlesContext())
                {
                    ctx.Configuration.ProxyCreationEnabled = false;
                    var titles = ctx.Titles.ToList();
                    return titles;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }                           
        }

        public IEnumerable<Title> GetSpecificTitles(string title)
        {
            try
            {
                using (var ctx = new TurnerTitlesContext())
                {
                    ctx.Configuration.ProxyCreationEnabled = false;
                    var searchedTitles = ctx.Titles.Where(x => x.TitleName.Contains(title)).OrderBy(x => x.TitleNameSortable).ToList();
                    return searchedTitles;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }
        [Route("api/Titles/Awards")]
        [HttpGet]
        public IEnumerable<Award> Awards(int titleId)
        {
            try
            {
                using (var ctx = new TurnerTitlesContext())
                {
                    ctx.Configuration.ProxyCreationEnabled = false;
                    var awards = ctx.Awards.Where(x => x.TitleId == titleId && x.AwardWon == true).ToList();
                    return awards;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("api/Titles/StoryLines")]
        [HttpGet]
        public IEnumerable<StoryLine> StoryLines(int titleId)
        {
            try
            {
                using (var ctx = new TurnerTitlesContext())
                {
                    ctx.Configuration.ProxyCreationEnabled = false;
                    var storylines = ctx.StoryLines.Where(x => x.TitleId == titleId).ToList();
                    return storylines;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("api/Titles/Genres")]
        [HttpGet]
        public IEnumerable<Genre> Genres(int titleId)
        {
            try
            {
                using (var ctx = new TurnerTitlesContext())
                {
                    ctx.Configuration.ProxyCreationEnabled = false;
                    var genre = (from tg in ctx.TitleGenres
                                 join t in ctx.Titles on tg.TitleId equals t.TitleId
                                 join g in ctx.Genres on tg.GenreId equals g.Id
                                 where t.TitleId == titleId
                                 select new { Id = g.Id, Name = g.Name }).ToList()
                                 .Select(x => new Genre { Id= x.Id, Name=x.Name});
                    return genre;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("api/Titles/Participants")]
        [HttpGet]
        public IEnumerable<Participant> Participants(int titleId)
        {
            try
            {
                using (var ctx = new TurnerTitlesContext())
                { 
                    ctx.Configuration.ProxyCreationEnabled = false;
                    var participants = (from tp in ctx.TitleParticipants
                                        join t in ctx.Titles on tp.TitleId equals t.TitleId
                                        join p in ctx.Participants on tp.ParticipantId equals p.Id
                                        where tp.IsKey == true && t.TitleId == titleId
                                        select new { Id = p.Id, Name = p.Name, ParticipantType = p.ParticipantType }).ToList()
                                        .Select(x => new Participant { Id = x.Id, Name = x.Name, ParticipantType = x.ParticipantType });
                    return participants;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
