﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TurnerTitles.Startup))]
namespace TurnerTitles
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
